import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { Configuration } from './config';

@Injectable()
export class ConfigService {
    private config: Configuration;

    constructor(private http: Http) {}

    load(url: string) {
        return new Promise((resolve) => {
            this.http.get(url).subscribe(config => {
                // this.config.webApiBaseUrl = config.json()['API_ENDPOINT_1'];
                this.config = new Configuration(config.json()['API_ENDPOINT_1']);
                resolve();
            });
        });
    }

    getConfiguration(): Configuration {
        return this.config;
    }
}
