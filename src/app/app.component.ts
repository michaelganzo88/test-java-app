import { Component } from '@angular/core';
import { NgbModule, NgbButtonsModule } from '@ng-bootstrap/ng-bootstrap';

import { Member, SumOperation } from './operation-member';
import { UnitPound, UnitScellino, UnitPence } from './unit-member';

import { OldfashionpoundService } from './oldfashionpound.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Old Fashion Pound';
  constructor(
    private oldFashionPoundService: OldfashionpoundService
  ) {}

  getSomma(): void {
    this.oldFashionPoundService.getSomma(new SumOperation()).then((res: Member) => {
      console.log(res);
    })
  }

}
