export class UnitMember {
    id: string;
    value: number;
    maxValue: number;
}

export class UnitPound extends UnitMember {
    id = 'p';
    maxValue = Number.MAX_VALUE;
}

export class UnitScellino extends UnitMember {
    id = 's';
    maxValue = 20;
}

export class UnitPence extends UnitMember {
    id = 'd';
    maxValue = 12;
}
