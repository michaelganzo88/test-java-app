import { UnitPound, UnitScellino, UnitPence } from './unit-member';

export class Price {
    pound: UnitPound;
    scellino: UnitScellino;
    pence: UnitPence;
}

export class Member {
    pound: number;
    scellino: number;
    pence: number;
}

export interface OperationInterface {}

abstract class Operation implements OperationInterface {
    firstValue: Member;
}

export class SumOperation extends Operation {
    secondValue: Member;
}

export class SubtractionOperation extends Operation {
    secondValue: Member;
}

export class MultiplicationOperation extends Operation {
    secondValue: number;
}

export class DivisionOperation extends Operation {
    secondValue: number;
}
