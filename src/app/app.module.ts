import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpModule } from '@angular/http';
import { routing, appRoutingProvider } from './app.routes';

import { AppComponent } from './app.component';

import { ConfigService } from '../config.service';
import { ConfigLoader } from './config-loader';

import { OldfashionpoundService } from './oldfashionpound.service';
import { OperationComponent } from './operation/operation.component';
import { OperationDetailComponent } from './operation-detail/operation-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    OperationComponent,
    OperationDetailComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    routing,
    HttpModule
  ],
  providers: [
    ConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: ConfigLoader,
      deps: [ConfigService],
      multi: true
    },
    appRoutingProvider,
    OldfashionpoundService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
