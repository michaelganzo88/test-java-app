import { TestBed, inject } from '@angular/core/testing';

import { OldfashionpoundService } from './oldfashionpound.service';

describe('OldfashionpoundService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OldfashionpoundService]
    });
  });

  it('should be created', inject([OldfashionpoundService], (service: OldfashionpoundService) => {
    expect(service).toBeTruthy();
  }));
});
