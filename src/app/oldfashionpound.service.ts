import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { Member, OperationInterface } from './operation-member';

@Injectable()
export class OldfashionpoundService {
  webApiBaseUrl: string;
  operation: OperationInterface;
  firstMember: Member;
  secondMember: Member;

  constructor(
      private http: Http,
      // private configService: ConfigService
  ) {
      // this.webApiBaseUrl = configService.getConfiguration().webApiBaseUrl;
      this.webApiBaseUrl = 'http://localhost:8080';
  }

  getSomma(operation: OperationInterface): Promise<Member> {
      return this.http.post(this.webApiBaseUrl + '/somma', operation)
      .toPromise()
      .then(response => {
          return response.json() as Member;
      })
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
      console.error('An error occurred', error); // for demo purposes only
      return Promise.reject(error.message || error);
  }
}
