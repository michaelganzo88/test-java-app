import { environment } from '../environments/environment';
import { ConfigService } from '../config.service';

export function ConfigLoader(configService: ConfigService) {
    return () => configService.load(environment.configFile);
}
